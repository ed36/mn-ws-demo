#!/bin/bash

single() {
  http -v :8080/echo
}

batch() {
  http -v :8080/echo/batch
}

t='{"name":"temperature","value":42.0}'
t1='[{"name":"temperature","value":42.0}]'
t2='[{"name":"temperature","value":42.0},{"name":"pressure","value":84.0}]'

# regular HTTP
echo $t   | single
echo $t1  | single

echo $t1  | batch
echo $t2  | batch

#  Websocket client https://github.com/vi/websocat#installation

ws_single() {
  websocat ws://localhost:8080/ws/echo
}

ws_batch() {
  websocat ws://localhost:8080/ws/echo/batch
}

# using websockets
echo $t   | ws_single
echo $t1  | ws_single

echo $t1  | ws_batch
echo $t2  | ws_batch

