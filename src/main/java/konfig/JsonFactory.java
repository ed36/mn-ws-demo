package konfig;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Replaces;
import io.micronaut.jackson.JacksonConfiguration;
import io.micronaut.jackson.ObjectMapperFactory;

import javax.annotation.Nullable;
import javax.inject.Singleton;

@Factory
@Replaces(ObjectMapperFactory.class)
class JsonFactory extends ObjectMapperFactory {

    @Override
    @Singleton
    @Replaces(ObjectMapper.class)
    public ObjectMapper objectMapper(@Nullable final JacksonConfiguration jacksonConfiguration, @Nullable final com.fasterxml.jackson.core.JsonFactory jsonFactory) {
        return super.objectMapper(jacksonConfiguration, jsonFactory)
                .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }
}

