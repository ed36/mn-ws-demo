package ws;

import io.micronaut.http.annotation.Body;
import io.micronaut.websocket.WebSocketSession;
import io.micronaut.websocket.annotation.OnClose;
import io.micronaut.websocket.annotation.OnMessage;
import io.micronaut.websocket.annotation.OnOpen;
import io.micronaut.websocket.annotation.ServerWebSocket;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static ws.MyMessage.listToString;

@ServerWebSocket("/ws/echo/batch")
public class WebsocketsBatchEcho {

    private static final Logger log = LoggerFactory.getLogger(WebsocketsBatchEcho.class);

    @OnOpen
    public Publisher<String> onOpen(WebSocketSession session) {
        return session.send(">>Connected");
    }

    @OnClose
    public Publisher<String> onClose(WebSocketSession session) {
        return session.send("Disconnected<<");
    }

    @OnMessage
    public Publisher<String> echo(@Body final List<MyMessage> msgs, WebSocketSession session) {
        return session.send(listToString(msgs));
    }

}
