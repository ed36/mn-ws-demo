package ws;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.annotation.Body;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter(onMethod_ = @JsonProperty)
@FieldDefaults(makeFinal = true, level = AccessLevel.PUBLIC)
@ToString
@EqualsAndHashCode
@Builder
@With
@Introspected
public class MyMessage {
    String name;
    Double value;

    public static String listToString(@Body final List<MyMessage> messages) {
        final var list = messages.stream().map(MyMessage::toString).collect(Collectors.toList());
        return "[" + StringUtils.join(list, ",") + "]";
    }
}
