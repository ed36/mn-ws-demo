package ws;

import io.micronaut.runtime.Micronaut;

public class DemoApp {

    public static void main(final String[] args) {
        Micronaut.run(DemoApp.class);
    }
}
