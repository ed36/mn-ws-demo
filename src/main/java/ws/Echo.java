package ws;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;

import java.util.List;

import static ws.MyMessage.listToString;

@Controller
public class Echo {

    @Post("/echo")
    @Consumes
    @Produces(MediaType.TEXT_PLAIN)
    public String echo(@Body final MyMessage message) {
        return listToString(List.of(message));
    }

    @Post("/echo/batch")
    @Consumes
    @Produces(MediaType.TEXT_PLAIN)
    public String batch(@Body final List<MyMessage> messages) {
        return listToString(messages);
    }

}
